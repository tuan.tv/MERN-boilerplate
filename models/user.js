const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    username: String,
    password: String,
    email: {
        type: String,
        unique: [true, 'Email must unique']
    },
    createDate: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('User', userSchema)
