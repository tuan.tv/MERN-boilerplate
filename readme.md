1. Run `yarn` or `npm install` to install the dependencies
2. Run `yarn run dev` or `npm run dev` for development

- Make sure you connected to mongodb
- Recommend use __yarn version 0.2x__ or __npm version 6.x__
