var express = require('express')
var router = express.Router()
var User = require('../../models/user.js')

/* GET users listing. */
router.get('/', (req, res, next) => {
    User.find({}).then((users) => {
        res.send(users)
    })
})

router.post('/', (req, res, next) => {
    User.create(req.body).then((user) => {
        res.send(user)
    }).catch(next)
})

router.put('/:id', (req, res, next) => {
    const id = req.params.id
    User.findByIdAndUpdate({_id: id}, req.body).then(() => {
        User.findOne({_id: id}).then((user) => {
            res.send(user)
        }).catch(next)
    })
})

router.delete('/:id', (req, res, next) => {
    const id = req.params.id
    User.findByIdAndRemove({_id: id}).then((user) => {
        res.send(user)
    }).catch(next)
})

module.exports = router
