const path = require('path')
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require("extract-text-webpack-plugin")

const isProd = process.env.NODE_ENV === 'production'
const cssProd = ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: [{
        loader: 'css-loader', options: { sourceMap: true, minimize: true }
    },
    {
        loader: 'sass-loader'
    },
    {
        loader: 'import-glob-loader'
    }],
    publicPath: '/public'
})
const cssDev = ['style-loader', 'css-loader?sourceMap', 'sass-loader', 'import-glob-loader']
const cssConfig = isProd ? cssProd : cssDev

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './src/index.html'
})

const ExtractTextPluginConfig = new ExtractTextPlugin({
    filename: 'css/[name].css',
    disable: !isProd,
    allChunks: true
})

module.exports = {
    devServer: {
        contentBase: path.join(__dirname, 'public'),
        compress: true,
        hot: true,
        stats: 'errors-only',
        historyApiFallback: true
    },
    entry: ['babel-polyfill', './src/index.js'],
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'js/bundle.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            { test: /\.scss$/, use:  cssConfig },
            { test: /\.js$/, use: 'babel-loader', exclude: /node_modules/ },
            { test: /\.jsx$/, use: 'babel-loader', exclude: /node_modules/ },
            {
                test: /\.(jpe?g|png|gif|svg)$/i,
                use: [
                  'file-loader?name=images/[name].[ext]',
                  'image-webpack-loader?bypassOnDebug'
                ]
            },
            { test: /\.(woff2?)$/, use: 'url-loader?limit=10000&name=fonts/[name].[ext]' },
            { test: /\.(ttf|eot|otf)$/, use: 'file-loader?name=fonts/[name].[ext]' }
        ]
    },
    plugins: [
        HtmlWebpackPluginConfig,
        ExtractTextPluginConfig,
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NamedModulesPlugin()
    ]
}
